from OpenGL.GL import *
from OpenGL.arrays.vbo import VBO
from OpenGL.GL.shaders import *

import numpy as np

from mat4 import *

class Scene:
    """
        OpenGL scene class that render a RGB colored tetrahedron.
    """

    def __init__(self, width, height, mesh, scenetitle="Duncans try angle"):
        self.scenetitle = scenetitle
        self.width = width
        self.height = height
        self.indices = []
        self.mesh = mesh

        self.animate = False
        self.angle = 0
        self.angle_increment = 1

        self.wireframe = False
        self.perspective = True

        self.flagRotateX = False
        self.flagRotateY = False
        self.flagRotateZ = False

        self.angleX = 0
        self.angleY = 0
        self.angleZ = 0
        self.angle_increment_axis_rotation = 10 # Const

        self.mouseMoveTo = [0, 0]

        self.zoom = 1

        self.arcball = rotate(0, np.array([1, 0, 0])) # set rotation to no effective
        self.arcball_angle = 0
        self.lastPos = [0, 0]


    def init_GL(self):
        # setup buffer (vertices, colors, normals, ...)
        self.gen_buffers()

        # setup shader
        glBindVertexArray(self.vertex_array)
        vertex_shader = open("shader.vert", "r").read()
        fragment_shader = open("shader.frag", "r").read()
        vertex_prog = compileShader(vertex_shader, GL_VERTEX_SHADER)
        frag_prog = compileShader(fragment_shader, GL_FRAGMENT_SHADER)
        self.shader_program = compileProgram(vertex_prog, frag_prog)

        # unbind vertex array to bind it again in method draw
        glBindVertexArray(0)

    def gen_buffers(self):
        # TODO:
        # 1. Load geometry from file and calc normals if not available -> Is done in mesh.py
        # 2. Load geometry and normals in buffer objects -> DONE


        # generate vertex array object
        self.vertex_array = glGenVertexArrays(1) # index of openGl buffer
        glBindVertexArray(self.vertex_array)

        # generate and fill buffer with vertex positions (attribute 0)
        positions = np.array([self.mesh.points], dtype=np.float32)
        pos_buffer = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, pos_buffer)
        glBufferData(GL_ARRAY_BUFFER, positions.nbytes, positions, GL_STATIC_DRAW)
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, None)
        glEnableVertexAttribArray(0)

        # generate and fill buffer with vertex colors (attribute 1)
        colors = np.array([self.mesh.color], dtype=np.float32)
        col_buffer = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, col_buffer)
        glBufferData(GL_ARRAY_BUFFER, colors.nbytes, colors, GL_STATIC_DRAW)
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, None)
        glEnableVertexAttribArray(1)

        # generate index buffer (for triangle strip)
        self.indices = np.array([self.mesh.indices], dtype=np.int32)
        ind_buffer_object = glGenBuffers(1)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ind_buffer_object)
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, self.indices.nbytes, self.indices, GL_STATIC_DRAW)

        # unbind buffers to bind again in draw()
        glBindBuffer(GL_ARRAY_BUFFER, 0)
        glBindVertexArray(0)

    def set_size(self, width, height):
        self.width = width
        self.height = height

    def draw(self):
        # TODO: alot of things, but only did like half of them

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        if self.animate:
            # increment rotation angle in each frame
            self.angle += self.angle_increment

        if self.flagRotateX:
            self.angleX += self.angle_increment_axis_rotation
            self.flagRotateX = False

        if self.flagRotateY:
            self.angleY += self.angle_increment_axis_rotation
            self.flagRotateY = False

        if self.flagRotateZ:
            self.angleZ += self.angle_increment_axis_rotation
            self.flagRotateZ = False

        # setup matrices
        if self.perspective:
            projection = perspective(45.0, self.width/self.height, 1.0, 5.0)
        else:
            projection = ortho(-1, 1, -1, 1, 1.0, 5.0)
        view = look_at(0, 0, 2, 0, 0, 0, 0, 1, 0)
        animation = rotate_y(self.angle) # because cows like to be spun
        model_translate = translate(self.mouseMoveTo[0], self.mouseMoveTo[1], 0)  # Flipping fucking Cows are fun!
        model_rotate = model_translate @ rotate_y(self.angleY) @ rotate_x(self.angleX) @ rotate_z(self.angleZ)
        model_sizing = model_rotate @ scale(self.zoom, self.zoom, self.zoom)
        model_transformed = model_sizing @ self.arcball
        mvp_matrix = projection @ view @ model_transformed @ animation

        # enable shader & set uniforms
        glUseProgram(self.shader_program)

        # determine location of uniform variable varName
        varLocation = glGetUniformLocation(self.shader_program, 'modelview_projection_matrix')
        # pass value to shader
        glUniformMatrix4fv(varLocation, 1, GL_TRUE, mvp_matrix)

        # enable vertex array & draw triangle(s)
        glBindVertexArray(self.vertex_array)

        if self.wireframe:
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)
        else:
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)

        glDrawElements(GL_TRIANGLES, self.indices.nbytes // 4, GL_UNSIGNED_INT, None)

        # unbind the shader and vertex array state
        glUseProgram(0)
        glBindVertexArray(0)

