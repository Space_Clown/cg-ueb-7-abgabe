import sys
import glfw

from const import EXIT_FAILURE

from OpenGL.GL import *
from OpenGL.arrays.vbo import VBO
from OpenGL.GL.shaders import *

import numpy as np
from mat4 import rotate


class RenderWindow:
    """
        GLFW Rendering window class
    """

    def __init__(self, scene):
        # initialize GLFW
        if not glfw.init():
            sys.exit(EXIT_FAILURE)

        # request window with old OpenGL 3.2
        glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)
        glfw.window_hint(glfw.OPENGL_FORWARD_COMPAT, glfw.TRUE)
        glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 3)
        glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 2)

        # make a window
        self.width, self.height = scene.width, scene.height
        self.aspect = self.width / self.height
        self.window = glfw.create_window(self.width, self.height, scene.scenetitle, None, None)
        if not self.window:
            glfw.terminate()
            sys.exit(EXIT_FAILURE)

        # Make the window's context current
        glfw.make_context_current(self.window)

        # initialize GL
        self.init_GL()

        # Flag for cursor
        self.moveObj = False
        self.zoomObj = False
        self.arcball = False

        # set window callbacks
        glfw.set_mouse_button_callback(self.window, self.on_mouse_button)
        glfw.set_key_callback(self.window, self.on_keyboard)
        glfw.set_window_size_callback(self.window, self.on_size)
        glfw.set_cursor_pos_callback(self.window, self.on_mouse_move) # callback for mouse position
        glfw.set_scroll_callback(self.window, self.size_scroll) # callback for mouse wheel... because i can


        # create scene
        self.scene = scene
        if not self.scene:
            glfw.terminate()
            sys.exit(EXIT_FAILURE)

        self.scene.init_GL()

        # exit flag
        self.exitNow = False

    def init_GL(self):
        # debug: print GL and GLS version
        # print('Vendor       : %s' % glGetString(GL_VENDOR))
        # print('OpenGL Vers. : %s' % glGetString(GL_VERSION))
        # print('GLSL Vers.   : %s' % glGetString(GL_SHADING_LANGUAGE_VERSION))
        # print('Renderer     : %s' % glGetString(GL_RENDERER))

        # set background color to black
        glClearColor(0, 0, 0, 0)

        # Enable depthtest
        glEnable(GL_DEPTH_TEST)

    # funny function from exercise sheet
    def projectOnSphere(self, x, y, r):
        x, y = x - self.width/2, self.height/2.0 - y
        a = min(r**2, x**2 + y**2)
        z = np.sqrt(r**2 - a)
        l = np.sqrt(x**2 + y**2 + z**2)
        return x/l, y/l, z/l

    def on_mouse_button(self, win, button, action, mods):
        print("mouse button: ", win, button, action, mods)

        if button == 0:
            pos = glfw.get_cursor_pos(win)
            # Save position to make a direction Vector for calc the angle
            self.scene.lastPos = [pos[0], pos[1]]
            self.arcball = not self.arcball
        if button == 1:
            self.moveObj = not self.moveObj
        if button == 2:
            self.zoomObj = not self.zoomObj

    def on_keyboard(self, win, key, scancode, action, mods):
        print("keyboard: ", win, key, scancode, action, mods)
        if action == glfw.PRESS:
            # ESC to quit
            if key == glfw.KEY_ESCAPE:
                self.exitNow = True
            if key == glfw.KEY_A:
                self.scene.animate = not self.scene.animate
            if key == glfw.KEY_P:
                print("toggle projection: orthographic / perspective ")
                self.scene.perspective = not self.scene.perspective
            if key == glfw.KEY_S:
                print("toggle shading: wireframe, grouraud, phong")
                self.scene.wireframe = not self.scene.wireframe
            if key == glfw.KEY_X:
                print("rotate: around x-axis")
                self.scene.flagRotateX = True
            if key == glfw.KEY_Y:
                print("rotate: around y-axis")
                self.scene.flagRotateY = True
            if key == glfw.KEY_Z:
                print("rotate: around z-axis")
                self.scene.flagRotateZ = True

    def on_size(self, win, width, height):
        self.scene.set_size(width, height)

    # You are asking why? because i fucking can! and it's UI intuitive thing
    def size_scroll(self, win, xoffset, yoffset):
        if yoffset > 0:
            self.scene.zoom += 0.1
        elif yoffset < 0 and self.scene.zoom > 0.1:
            self.scene.zoom -= 0.1


    def on_mouse_move(self, win, posX, posY):
        if self.moveObj:
            # TODO Function that stops cow from beeing moved out of the window? usefully or useless? who the fuck needs cos anyway?!
            self.scene.mouseMoveTo = [((posX - (self.width/2))/self.width) , ((-posY + (self.height/2)) / self.height)]

        if self.zoomObj and self.scene.zoom > 0.1 and posY > 0:
            # TODO if getting close to 0 it breaks
            self.scene.zoom = posY/100

        if self.arcball:

            radius = min(self.width, self.height)
            pos_start = self.projectOnSphere(self.scene.lastPos[0], self.scene.lastPos[1], radius)
            pos_end = self.projectOnSphere(posX, posY, radius)
            rotation_axis = np.cross(pos_start, pos_end)

            normalized_start = pos_start / np.linalg.norm(pos_start)
            normalized_end = pos_end / np.linalg.norm(pos_end)

            pos_skal = np.dot(normalized_start, normalized_end) # didnt fucking work to normalize vectors.. back to stupid way...

            if pos_skal > 1:
                angle = np.arccos(1)
            elif pos_skal < -1:
                angle = np.arccos(-1)
            else:
                angle = np.arccos(pos_skal)

            self.scene.arcball_angle += angle

            rotation_arcball = rotate(self.scene.arcball_angle, rotation_axis)

            self.scene.arcball = rotation_arcball
            # self.scene.lastPos = [posX, posY]


    def run(self):
        while not glfw.window_should_close(self.window) and not self.exitNow:
            # poll for and process events
            glfw.poll_events()

            # setup viewport
            width, height = glfw.get_framebuffer_size(self.window)
            glViewport(0, 0, width, height);

            # call the rendering function
            self.scene.draw()

            # swap front and back buffer
            glfw.swap_buffers(self.window)

        # end
        glfw.terminate()
