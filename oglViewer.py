import sys

from window import RenderWindow
from scene import Scene
from mesh import Mesh
from const import *

# main function
if __name__ == '__main__':

    print("presse 'a' to toggle animation...")

    filename = sys.argv[1]

    mesh = Mesh(f'{FILEPATH}{filename}')

    # set size of render viewport
    width, height = 640, 480

    # instantiate a scene
    scene = Scene(width, height, mesh)

    # pass the scene to a render window ...
    rw = RenderWindow(scene)

    # ... and start main loop
    rw.run()
