import numpy as np

from mat4 import translate, scale


class Mesh(object):
    def __init__(self, filename):
        self.filename = filename
        self.vertices = []
        self.faces = []
        self.texturkoordinaten = []
        self.verticesTrans = []

        self.color = []
        self.points = []
        self.indices = []
        self.normals = [] # just calculate them every fucking time because of bug'ish things
        self.boundries = []

        try:
            f = open(filename)
            for line in f:
                if line.startswith('v '):
                    vertex = []
                    for y in line.split()[1:]:
                        vertex.append(float(y))
                    vertex.append(float(1))
                    self.vertices.append(np.array(vertex))

                lineConverted = line.replace("\n", "").split(" ")
                if lineConverted[0] == "vt":
                    lineSplit = line.replace("\n", "").split(" ")
                    lineClean = []
                    for ele in lineSplit:
                        if len(ele.replace(" ", "")) != 0:
                            lineClean.append(ele)
                    texture = (float(lineClean[1]), float(lineClean[2]), float(lineClean[3]))
                    self.texturkoordinaten.append(texture)

                if lineConverted[0] == "f":
                    del lineConverted[0]
                    face = []
                    # set -1 as Flag for no Objekt found
                    for element in lineConverted:
                        if element.__contains__("/"):
                            x = element.split('/')
                            self.indices.append(int(x[0]) - 1)
                            if len(x[1]) < 1:
                                facetuples = (int(x[0]) - 1, -1, int(x[2]) - 1)
                            else:
                                facetuples = (int(x[0]) - 1, int(x[1]) - 1, int(x[2]) - 1)
                            face.append(facetuples)
                        else:
                            facetuples = (int(element) - 1, -1, -1)
                            face.append(facetuples)
                            self.indices.append(int(element) - 1)

                        face.sort(key=lambda tup: tup[0])
                    self.faces.append(face)

            f.close()

            self.calcbBundries()
            self.scaleAndTranslateToOrigin() # always generate normals because sometimes the fucking files come without them ... fucking why... i hate my life
            self.calculateColor()

        except IOError:
            print("Didnt find Obj.")

    def calcbBundries(self):
        for x in range(3):
            self.boundries.append(min(self.vertices, key=lambda vertice: vertice[x])[x])
            self.boundries.append(max(self.vertices, key=lambda vertice: vertice[x])[x])

    def scaleAndTranslateToOrigin(self):
        moveToStart = translate(-(self.boundries[0] + self.boundries[1]) / 2, -(self.boundries[2] + self.boundries[3]) / 2, -(self.boundries[4] + self.boundries[5]) / 2)
        windowSizeMatrix = min([1/np.abs(self.boundries[1] - self.boundries[0]), 1/np.abs(self.boundries[3] - self.boundries[2]), 1/np.abs(self.boundries[5] - self.boundries[4])])
        scaleMatrix = scale(windowSizeMatrix, windowSizeMatrix, windowSizeMatrix)
        m = scaleMatrix @ moveToStart
        self.verticesTrans = [(m @ v)[:-1] for v in self.vertices]
        self.calculateNorms()
        self.convertVertices()

    def calculateNorms(self):
        for i in range(len(self.faces)):
            face = self.faces[i]
            pointA = self.verticesTrans[face[0][0]]
            vectoreBA = self.verticesTrans[face[1][0]] - pointA
            vectoreCA = self.verticesTrans[face[2][0]] - pointA

            normalVectorABC = np.cross(vectoreBA, vectoreCA)
            self.normals.append(normalVectorABC / np.linalg.norm(normalVectorABC))


    def calculateColor(self):
        # To have a red object
        for x in range(int(len(self.points) / 3)):
            self.color.append(1.0)
            self.color.append(0.0)
            self.color.append(0.0)

    def convertVertices(self):
        self.points = np.array([x for point in self.verticesTrans for x in point], dtype=np.float32)







